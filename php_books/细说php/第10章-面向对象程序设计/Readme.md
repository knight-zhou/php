## 对象中成员的访问
```
class Person(){
	var $name;       // 用var 声明成员属性，没有var修饰就是public
	var $sex;  
	var $age;

function run(){
	echo "正在奔跑....";
}
person1-> = new person();   //实例化
$person2 = new person();   //实例化

$person1->name = "张三";   // 对name属性赋值

$person2->name = "李四";

echo "对象1名字是:".$person1->name       // 成员属性访问
echo "对象2名字是:".$person2->name       // 成员属性访问

}
```
## 构造方法
对象创建时候也就是new的时候会自动调用一次
```
<?php
class Peron(){
	var $name;
	var $sex;
	var $age;     // 定义成员变量
	function __construct($name="",$sex="man",$age=1){
		$this->name=$name;
	}

	function say(){
		echo $this->name;    
	}

	$person1 = new Person('张三','man','20');   //实例化
	$person1->say();   
}
```
## 析构方法`__destruct()`
对象被销毁钱自动调用该方法，不常用

## 封装性
可以使用private 关键字修饰就实现了成员的封装

## 魔法方法
像构造方法和析构方法都是魔法方法，另外还有四个魔法方法

* __set()

* __get()

* __isset()

* __unset()

## 继承和重写
继承就不再多说 很简单，对于重写，如果子类重写了父类的方法 ，去访问子类的方法肯定是重写了的方法，如果再要去访问父类原本的方法呢？
使用 `parent::方法名` 即可.

## 常见的关键字
这些都是为了提高对象或者类的应用能力
* final 关键字
* static 关键字
* const 关键字
* instanceof 关键字  //这个用得比较少

## 常用的魔术方法
* __call()

* __toString()

* __autoload()

## 抽象类与接口
抽象类的作用是对于架构师很有用，架构师一般写好核心的抽象方法，定义整个业务系统架构

```
abstract function fun1();     // 没有花括号 没有函数方法
abstract function fun2();     // 没有花括号 没有函数方法
```

对于接口：
接口就是一种特殊的抽象类
```
interface One{                     // 用interface修饰,没有圆括号
	functon fun1();              // 定义抽象方法
	functon fun2();

}
```
## 多态性
对方法重写并能增加新的功能 也是多态的表现之一
```
interface USB{
	function run();
}

// 声明一个计算机类去使用usb设置
class Computer{
	function useUSB($useb){
	$sub->run();
}
}

$co = new Comuter;   //实例化计算机类
$co->useUSB(new mouse());  // 插入鼠标
$co->useUSB(new ukey());  // 插入ukey
```
以上就是重写了run方法，并插入不同的东西有不同的形态

当然你还可以实现更多的USB设置
```
class Ukey implements USB{
	function run(){
	echo "运行usb设备";
}
class Umouse implements USB{
	function run(){
	echo "运行鼠标设备";
}
}

}
```