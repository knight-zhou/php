## 静态变量
```
function test(){
	static $a=0;
	echo $a;
	$a++;
}
test();   // 输出0
test();  // 输出1
test();
```
## 应用各种形式的PHP函数
### 默认参数的函数
```
function person($name="xx",$age=20){
	echo $name;
	echo $age;	
}
person();
person('李四');
person('李四',18);

```

### 可变个数的函数参数
使用…$args代替任意数量参数
```
function myfunc(...$args){

    // 获取参数数量
    echo count($args).PHP_EOL;

    // 获取第一个参数的值：
    print_r($args[0]);
    echo PHP_EOL;

    // 获取所有参数的值
    print_r($args);
    echo PHP_EOL;

}

myfunc('a');
myfunc(1, 2, 3);
myfunc(array('d','e'), array('f'));

?>
```

### 回调函数
`call_user_func_array()` 是一个内置函数 也就是一个回调函数

作用:我们都知道在js中经常处理异步请求的时候,如果使用普通的函数,可能无法保证异步请求完成后调用。所以就存在了callback函数,特别是在文件处理和ajax处理的时候,回调函数的作用就非常的大了。
```
function foo($n, $f='') {
  if($n < 1) return;
  for($i=0; $i<$n; $i++) {
    echo $f ? $f($i) : $i;
  }
}
//无回调时
foo(5); //01234
 
//有回调时
function f1($v) {
  return $v + $v;
}
foo(5, 'f1'); //02468

``` 

检查函数是否存在，如果存在,那么就调用该函数.同时将参数附加进去.
```
<?php
function invoke($name){
  if(function_exists($name)){
     $args = array_slice(func_get_args(),0,1);
     call_user_func_array($name,$args);
  }
  die("no function");
}
 
function test(){
    echo 1;
}
invoke("test");   // 1
invoke("test2"); // no function

```
这里我们通过function_exists 来检测是否为一个函数.如果 为函数的话就立即调用函数。如果不为函数则die

### 递归函数
作用是精简程序中繁杂的重复调用程序，提高效率
```
function test($n){
	echo $n;
	
	if($n>0){
		test($n-1);	
	}else{
		echo '<---->';
	}
	
	echo $n;
}

test(10);
```