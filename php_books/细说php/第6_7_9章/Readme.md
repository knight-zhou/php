### 特殊的流程控制语句
* break 跳出循环，可以指定跳出循环的层次
* continue  用于结束当次循环 进入下一个循环
* exit 退出脚本

## 流程控制语句
### switch语句
```
<?php
$favcolor="red";

switch ($favcolor) {
   case "red":
     echo "Your favorite color is red!";
     break;
   case "blue":
     echo "Your favorite color is blue!";
     break;
   case "green":
     echo "Your favorite color is green!";
     break;
   default:
     echo "Your favorite color is neither red, blue, or green!";
}
```

### while 循环
只要指定的条件为真，while 循环就会执行代码块。
```
<?php 
$x=1;
  
while($x<=5) {
   echo "数字是：$x <br>";
   $x++;
} 
```
### do...while 语句
do...while 循环首先会执行一次代码块，然后检查条件，如果指定条件为真，则重复循环。
```
<?php 
$x=1;

do {
   echo "数字是：$x <br>";
   $x++;
} while ($x<=5);

```
## 多条件if...else
```
<?php
$t=date("H");

if ($t<"10") {
   echo "Have a good morning!";
} elseif ($t<"20") {
   echo "Have a good day!";
} else {
   echo "Have a good night!";
}

```

## 数组
### 预定义数组
```
预定义数组	说明
$_SERVER	变量由web服务器设定或者直接与当前脚本的执行环境相关联
$_ENV	执行环境提交至脚本的变量
$_GET	经由URL请求提交至脚本的变量
$_POST	经由http post方法提交至脚本的变量
$_REQUEST	经由get、post和cookie机制提交至脚本的变量，因此该数组并不值得信任
$_FILES	经由http、post文件上传而提交至脚本的变量
$_COOKIE	经由http cookies方法提交至脚本的变量
$_SESSION	当前注册给脚本会话的变量
$GLOBALS	包含一个引用指向每个当前脚本的全局变量范围内有效的变量。该数组的键名为全局变量的名称

```
### 数组的键值操作
* array_values()
* array_keys()
* in_array() 判断是否存在
* array_flip() 反转数组
....
