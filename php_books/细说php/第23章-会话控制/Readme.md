## session
生成session
```
<?php
session_start();   // session 初始化

$_SESSION["username"]='knight'; 
$_SESSION["uid"]=1; 
```
销毁session
```
unset($_SESSION['username])   //
$_SESSION=array();  //销毁所有
```

## PHP header()函数
header()函数向客户端发送原始的 HTTP 报头
```php
<?php
// 在调用 header() 之前已存在输出
header('Location: http://www.example.com/');

```
 

