## 每天记录一点点
### 关于这3个符号
* => 是数组成员访问符号,也就是表示key 和value 键值对
* -> 是对象成员访问符号(用来引用对象的成员的变量与方法) ,也就是 "的" 的意思
```
<?php 
$arr=['a'=>123,'b'=>456];//数组初始化 
print_r($arr);//查看数组 

class A{ 
    public $a="xxx"; 

    public $b="yyy"; 
} 
print_r("######################");
$obj=new A();
print_r("-------------");
echo $obj->a;   //对象引用
print_r("____________");
print_r($obj);//查看对象
```
*  `::`双冒号操作符=作用域限定操作符,可以访问静态、const(常量)和类中重写的属性与方法(也就是调用父类方法)
[请看链接](http://www.php.cn/php-weizijiaocheng-378631.html)

### 单引号和双引号的区别
```
一般情况下两者是通用的.但双引号内部变量会解析,单引号则不解析
```
### PHP include 和 require 语句的区别
include 和 require 语句功能是相同的
* require()函数是指包含进来的内容被当成当前文件的一个组成部分,所以当包含进来的文件有语法错误或者文件不存在的时候,那当前文件的PHP脚本都不再执行。

* include()函数相当于指定这个文件的路径,当被包含的文件有错时,不会影响到本身的程序运行

### php的连接符
php的连接符是 点号("."),python是用加号("+").

### 使用大括号标准传参
```
if (!empty($_GET['keys'])){
    // $sql = "select * from news where $w order by id desc limit 1";
    $keys=$_GET['keys'];
    $w = "title like '%{$keys}%'";
    // echo $w;

}else{
    $w =1;

}
```

#### php双冒号使用场景
* 当调用静态属性和静态方法时(与->的作用相同，只不过使用的对象不一样！::引用类里面的静态方法或者属性，而且不需要实例化！)
```
class test {
    static public $a;
    static public function b() {}
}
```
如果要调用静态属性$a，如下:
`test::$a;`

注意，a前面需要带$符号，这一点与->符号不一样！如果要调用静态方法b，如下
`test::b();`

* 调用自身类或者父类的属性或者方法时
```
class test {
    public function b() {}
}
// 继承父类
class tests extends test {
    public function cs() {}
}
```
当我们需要调用父类的方法b时:
`parent::b();`

当我们需要调用自身的方法cs时，有两种方法:
```
$this->cs();
or
self::cs();
```
#### php单引号和双引号的区别
```
单引号内部变量不会执行
双引号内部变量会执行
```
#### php命名空间
[php命名空间官网](http://www.php.net/manual/zh/language.namespaces.php)

#### $this 和self的区别
```php
self代表本类，$this代表实例化后的对象
使用场景:  
(1)如果从类的内部访问 类的属性或者方法 则必须使用self关键字, 
例如： self::test();
(2)如果从类的外部访问 类的属性或者方法 则必须使用$this关键字
例如：  $this->test();

```
####  不依赖nginx的php web
php -S localhost:8000

or

Workerman/swoole
