package main

import "fmt"

func main() {
	var sum int64 = 0
	for i := 0; i < 10000; i++ {
		sum = sum + int64(i)
	}
	fmt.Print(sum)
}
