<?php
// 优先加载Common下的配置文件
return array(
	// 'URL_MODEL' => 1,
	'DB_TYPE'			=>	'mysql',
	'DB_HOST'			=>	'localhost',
	'DB_NAME'			=>	'thinkphp_blog',  //需要新建一个数据库！
	'DB_USER'			=>	'blog',		//数据库用户名	
	'DB_PWD'			=>	'blog',    //数据库登录密码
	'DB_PORT'			=>	'3306',
	'DB_PREFIX'		=>	'think_',   //数据库表名前缀


);