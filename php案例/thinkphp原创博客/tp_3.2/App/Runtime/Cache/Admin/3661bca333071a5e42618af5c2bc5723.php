<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo ($title); ?></title>

<link rel="stylesheet" href="/tp32_blog/Public/Css/admin/reset.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/tp32_blog/Public/Css/admin/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/tp32_blog/Public/Css/admin/invalid.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/tp32_blog/ueditor/themes/default/ueditor.css"/>

<script type="text/javascript" src="/tp32_blog/Public/Js/admin/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/tp32_blog/Public/Js/admin/simpla.jquery.configuration.js"></script>
<script type="text/javascript" src="/tp32_blog/Public/Js/admin/facebox.js"></script>
<script type="text/javascript" src="/tp32_blog/Public/Js/admin/jquery.wysiwyg.js"></script>

<script type="text/javascript" src="/tp32_blog/ueditor/editor_config.js"></script>
<script type="text/javascript" src="/tp32_blog/ueditor/editor_all.js"></script>

</head>

<body>
<div id="body-wrapper">

  <div id="sidebar">
    <div id="sidebar-wrapper">
     
      	<h1 id="sidebar-title"><a href="#">Simpla Admin</a></h1>
      	<img id="logo" src="/tp32_blog/Public/Images/admin/logo.png" alt="Simpla Admin logo" />

      	<div id="profile-links"> 
      		您好,<a href="#" title="当前用户:<?php echo ($username); ?>"><?php echo ($username); ?></a> |
	 		<a href="/tp32_blog/index.php/Admin/Index/quit" title="退出">退出</a> 
       	</div>
       	
    </div>
  </div>
   
  <div id="main-content">
  	<?php echo ($test); ?>
 
    
    <h2>新文章</h2>
	<br>
	

   	<form action="/tp32_blog/index.php/Admin/Index/add"  method="post">
   		<p class="subtit">文章标题</p>
		<div>
			<select id="selType">
				<option value="0">请选择</option>
				<option value="1">原创</option>
				<option value="2">转载</option>
				<option value="4">翻译</option>
			</select>
			<input type="text" id="txtTitle" name="subject" style="width:560px; height:20px; float:left;" maxlength="100" value="<?php echo ($article_item["subject"]); ?>"/>
		</div>
		<br>
		<p class="subtit">文章内容</p>
		
		<input type="text" name="message" style="width:1200px;height:400px">
	 
		<br>
		<input type="submit" value="发布">
	</form>
    <div class="clear"></div>
    
  </div>

</div>
</body>
</html>