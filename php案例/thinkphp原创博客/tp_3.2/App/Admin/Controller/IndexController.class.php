<?php
namespace Admin\Controller;
use Think\Controller;
// http://127.0.0.1/tp32_blog/index.php/admin/index/index

class IndexController extends Controller {
    // 后台首页函数
    public function index(){
        // 判断是否登陆,如果不存在cookie，就跳转到登陆界面
        $coo=cookie('name');
        /*
        if($coo){
            redirect(U('Admin/Login/index'));
        }
        */
            //实例化文章模型
            $news=M('article');	
            $count=$news->count();
            //转成sql为: select * from article order by id desc
            $news_list=$news->field(array('id','subject','author','createtime'))->order('id desc')->select();
        
                $username=cookie('username');  // 取session值
        
            // 分页代码暂时先省略
        
                $this->assign('news_count',$count)->assign('title','后台管理系统')->assign('test','testest');
                $this->assign('news_list',$news_list);
                $this->assign('username',$username);  // 读取session写入的用户 存入模板
                $this->display();  // 渲染到模板
        }

// 编辑文章页面
    public  function edit(){
    if($_GET['id']){
        $this->assign('title','哈哈');
        $this->display('Index/edit');
    }
    }

// 文章新增页面
    public function add(){
			// 写入数据
			//$article=D('Article');      // 写入一般用D方法(需要模型类),查询用M方法
			$article=M('Article');      // 写入一般用D方法(需要模型类),查询用M方法
			
		    $data=$_POST;            // 获取所有传过来的值
		    // print_r($data['subject']);

		    // 写入数据库
			if($article->add($data)){
				$this->success('文章添加成功，返回上级页面',U('Index/index'));

			}else{
				$this-error('文章添加失败');
			}
    }
    
// 文章删除界面
    public function delete(){
        $article=M('article');  // 实例化基础模型类
        if($article->delete($_GET['id'])){
            $this->success('文章删除成功');
        }else{
            $this->error($article->getLastSql());
    }
 }

// 登出节目
    public function quit(){
        cookie('username',null);  // 清除cookies
        print_r('您已经安全的登出');
    }


    }
