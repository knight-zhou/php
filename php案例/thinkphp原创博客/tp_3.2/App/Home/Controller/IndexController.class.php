<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index(){
        // 实例化文章模型
        $news = M('article');	  // 实例化基础模型类
        // 转化成sql为: select `id`,`subject`,`message`,`createtime` from think_article ORDER BY id desc;
        $news_list = $news->field(array('id','subject','message','createtime'))->order('id desc')->select();
    
        // 遍历每一条记录，然后取对应key值(也就是对应id的记录)的列值格式化输出;
    
        foreach ($news_list as $key => $value) {
            $news_list[$key]['createtime'] = date("Y-m-d H:i:s",$value['createtime']);
        }
    
        $this->assign('title','欢迎光临我的博客')->assign('news_list',$news_list);   // 存储到模板变量
        // $this->display('Index/index');   // 默认找模板下的同名模块文件夹下的index方法
        $this->display();   


        }

    }
