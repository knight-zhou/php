<?php
//index.php?m=user&a=add
$m=empty($_GET['m'])?'index':$_GET['m'];  //模块的名称
//模块的名称index
$a=empty($_GET['a'])?'index':$_GET['a'];  //方法的名称
// include "./controller/".$m."action.class.php";
//获取当前的记载路径
$include_path=get_include_path();
// 拼接所需要的加载路径
$include_path.=PATH_SEPARATOR."./model";
$include_path.=PATH_SEPARATOR."./controller";
$include_path.=PATH_SEPARATOR."./org";

//PATH_SEPARATOR 在linux上是一个":"号,WIN上是一个";"号
//set_include_path就是设置php的包含文件路径，相当是操作系统的环境变量
//get_include_path取得当前已有的环境变量
//设置加载路径
set_include_path($include_path);
//加载控制器文件
function __autoload($className){
    include strtolower($className).".class.php";
}

// 引入smarty引擎
// require './smarty/smarty.php';
require './smarty/libs/Smarty.class.php';


$m=ucfirst($m)."Controller";    //IndexController
$mod=new $m;
//$调用方法
$mod->$a();