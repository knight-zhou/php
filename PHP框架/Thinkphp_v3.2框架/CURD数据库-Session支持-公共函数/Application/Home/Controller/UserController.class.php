<?php
namespace Home\Controller;
use Think\Controller;
use Think\Log;

/*
访问： http://127.0.0.1/tp32/index.php/home/user/index
http://127.0.0.1/tp32/index.php/home/user/xieru

*/


class UserController extends Controller {
    public function index(){
        $this->show('这是home模块的User控制器');
    }
    
    // 查询数据库并返回json
    public function list_all(){
        $Student = M("Student"); // 实例化Student对象
        $list = $Student->getField('id,name,sex',':');
        $res_data = array("code"=>1001,"data"=>$list); 
        $res = array("res"=>$res_data);
        print_r(json_encode($res));   // encode：编码成json

    }

    // 接受post 传过来的数据并写入数据库
    public function xieru(){
        $Student = M("Student");
        $post_name  = $_POST['name'];
        $post_sex  = $_POST['sex'];
        $data["name"] = $post_name;
        $data['sex']= $post_sex;
        $Student->add($data);
        Log::write("xx","info");
  }

  
    
}
