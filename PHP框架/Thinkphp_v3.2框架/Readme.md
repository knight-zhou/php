## 以下两种url等效(其中c表示控制器,a表示方法)
* http://127.0.0.1/tp32/index.php/home/Blog/guidang/year/2017/month/10
* http://127.0.0.1/tp32/index.php/home/?c=Blog&a=guidang&year=2017&month=10

## 绑定模块
例如，我们定义了一个入口文件admin.php，希望可以直接访问Admin模块，那么我们就可以在admin.php中进行模块绑定，定义如下：
```
// 绑定访问Admin模块
define('BIND_MODULE','Admin');

// 定义应用目录define
define('APP_PATH','./Application/');
require './ThinkPHP/ThinkPHP.php';
```
在入口文件中绑定模块后，访问的URL地址中就不需要传入模块名称了

http://serverName/admin.php/Index/test/var/name  // 访问正常 
如果访问
http://serverName/admin.php/Admin/Index/test/var/name 就会出现下面的错误提示：找不到Admin模块

## M方法和D方法实际就是 new 实例化路径下的模型类的缩写
```
// 使用M方法实例化
$User = M('User');
// 和用法 $User = new \Think\Model('User'); 等效
```

## 模板中取session值得方法
* 后台写session 并传到前端
```
public function index(){
	session('name','xxxoooyyy'); 
	$this->assign('name',session('name'));
	$this->display();
}
```
* 使用系统变量输出，而不需要assign 和display
```
{$Think.session.user_id} // 输出$_SESSION['user_id']变量
{$Think.server.script_name} // 输出$_SERVER['SCRIPT_NAME']变量
{$Think.cookie.name}  // 输出$_COOKIE['name']变量

```
##这个机制可以使得模板文件的定义更加方便，默认的替换规则有：
```
__ROOT__: 会替换成当前网站的地址(不含域名)
__APP__: 会替换成当前应用的URL地址(不含域名)
__MODULE__: 会替换成当前模块的URL地址(不含域名)
__CONTROLLER__ (__或者__URL__ 兼容考虑):会替换成当前控制器的URL地址(不含域名)
__ACTION__: 会替换成当前操作的URL地址(不含域名)
__SELF__: 会替换成当前的页面
URL__PUBLIC__: 会被替换成当前网站的公共目录 通常是 /Public/
```
## 以下两种方法等效
```
$this->redirect('Admin/index/index');  // 使用跳转函数跳转到Admin模块下的Index控制器下的index方法

redirect(U('/Admin/index/index/')); // 或者使用redirect的U方法
```