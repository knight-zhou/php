<?php
namespace app\index\controller;
use think\Db;
/*

http://127.0.0.1/tp5/public/index.php/index/user/index

http://127.0.0.1/tp5/public/index.php/index/user/list_all

http://127.0.0.1/tp5/public/index.php/index/user/xieru
*/

Class User{
    public function index(){
        return "this index模块,user控制器的index方法";
    }
    
    // 查询数据
    public function list_all(){
        $data = Db::table("student")->select();
//        print_r($data);
        $res = array("code"=>1000,"data"=>$data);
        print_r(json_encode($res));
    }

    // 写入数据
    // 接受post 传过来的数据并写入数据库
    public function xieru(){
        $post_name  = $_POST['name'];
        $post_sex  = $_POST['sex'];
        $data = array("name"=>$post_name,"sex"=>$post_sex);
        Db::table("student")->insert($data);
        return "写入成功";

    }


}