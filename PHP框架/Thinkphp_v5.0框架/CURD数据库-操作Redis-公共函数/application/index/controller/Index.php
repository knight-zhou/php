<?php
namespace app\index\controller;
use tools;
/*
http://127.0.0.1/tp5/public/index.php/index/index


http://127.0.0.1/tp5/public/index.php/index/index

http://127.0.0.1/tp5/public/index.php/index/index/hello

http://127.0.0.1/tp5/public/index.php/index/index/xx

*/

use think\Cache;
use tools\exepython;

class Index
{
    public function index()
    {
        return 'this is .......';
    }

    public function hello(){
        return "this is abc....";
    }

    public function xx(){
        $data = array("name"=>"knight","city"=>"cs");
        return json_encode($data);
    }

    // 测试公共函数
    public function gg(){
        exe_shell();
    }
    // 测试redis缓存
    public function redis(){
        $redis = new \Redis();
        $redis->connect('192.168.0.53',6301);
        $redis->auth('zhoulong');
        // 写入
        $redis->set("sex",'manxx');

        // 读取
        $data = $redis->get('sex');
        print_r($data);
    }
    // 使用框架自带的redis类
    public  function tp_redist(){
        // 测试写入
        Cache::set("city",'guangzhou');
        // 测试读取
        $data = Cache::get("city");
        print_r($data);
    }

    // 测试自定义的lib库
    public  function  abc(){
        // 实例化自定义lib里的内容
        $xx = new exepython();
        $xx->exe_xx_python();
    }

}
