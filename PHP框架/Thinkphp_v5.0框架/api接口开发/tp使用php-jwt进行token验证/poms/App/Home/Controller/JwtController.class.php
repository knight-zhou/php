<?php
namespace Home\Controller;
use Think\Controller;
use \Firebase\JWT\JWT;   // 引入第三方库jwt

class JwtController extends Controller{
    public function index(){
        print_r("test jwt");
    }

    public function one(){
        $key = "abcdef";
        $token = array(
            'uid'=>1050,
            'username'=>'baby'
        );
        $jwt = JWT::encode($token, $key);   // 加密
        echo $jwt;
    }

    public function check_login(){
        $uname=I('post.username');
        $upwd=I('post.password');
        // 检测用户名和密码是否正确
        $find_record = M('Auth_user')->where("username='$uname' AND password='$upwd'")->find();
        // 
        $arr=array(
            "uid"=>$find_record['uid'],
            "username"=>$find_record['username']
        );
        // print_r(C('jwt_key'));
        //生成密钥
        $jwt = JWT::encode($arr, C('jwt_key'));
        $data=array('status'=>200,'msg'=>'sucess','token'=>$jwt);
        exit(json_encode($data));
    }

    public function user_info(){
        $token=I('post.token');
        // print_r($token);
        $key=C('jwt_key');

        // 进行解密
        if($token != ''){
            $obj_token = JWT::decode($token,$key, array('HS256'));
            $arr = json_decode(json_encode($obj_token), true);
            print_r($arr['uid']);
        }
        
    }

}