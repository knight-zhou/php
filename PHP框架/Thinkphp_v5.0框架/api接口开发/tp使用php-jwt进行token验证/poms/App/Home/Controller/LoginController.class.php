<?php
namespace Home\Controller;
use Think\Controller;
use Think\Log\Driver;

use \Firebase\JWT\JWT;   // 引入第三方库jwt

class LoginController extends Controller {
    public function index(){
        $this->display('login/login');
    }

    public function login_success(){
        $this->redirect('Home/index/index');
    }

    public function login(){
        $uname=I('post.username');
        $upwd=I('post.password');

        // 检查用户是否存在
        $user=M('Auth_user');
        $select=$user->query("select * from tk_auth_user where username='$uname' and password='$upwd'");
        $gid=$user->query("select gid from tk_auth_user where username='$uname'");
        $group=$gid[0]['gid'];

        if($select){
            session('uname', $uname);   // 写入session or cookie
            session('group', $group);   // 写入session or cookie
            $this->success('登陆成功', 'login_success');
        }else{
            $this->redirect('Home/login/index','',1,'用户名或密码错误!请重新输入,页面即将返回');
        }

    }

    public function quit(){
        session(null); // 清空当前所有的session
        $this->redirect('Home/login/index');
    }

    public function test(){
        Log::record('测试日志信息');
       
        // Think\Log::write('测试日志信息，这是警告级别，并且实时写入','WARN');
        // E('新增失败.........',25);   // 内置方法抛出异常
    }

}