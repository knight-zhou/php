<?php
namespace Home\Controller;
use Think\Controller;
use \Firebase\JWT\JWT;   // 引入第三方库jwt

// http://127.0.0.1/oms_platform/poms/index.php/home/test/index

class TestController extends Controller{
    // 初始化方法,执行控制器之前先验证令牌是否正确
    public function _initialize(){
        $token=I('post.token');
        \check_token($token);
    }

    public function index(){
        // 如果不加header直接输出json_encode的值的话，返回的是字符串不是对象
        header('Content-Type:application/json; charset=utf-8');
        $arr_person= array('name'=>'tom','sex'=>'man');
        exit(json_encode($arr_person));
    }

}