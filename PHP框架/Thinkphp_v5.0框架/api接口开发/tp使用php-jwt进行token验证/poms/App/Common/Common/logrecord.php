<?php
/*
 日志记录公共函数
*/

function LogRecord($logname,$data){
   // $log_file ='.\Web_log\\'.$logname."_".date('Ymd',time()).'.log';  //windows日志路径
    $log_file ='./Web_log/'.$logname."_".date('Ymd',time()).'.log';  //linux 日志路径
    $data=date('Y-m-d H:i:s',time()).' '.$data;          // 日志加时间等内容
    $content =var_export($data,TRUE);  // 数组转成字符串
   
    if(file_exists($log_file)){
        file_put_contents($log_file,$data.PHP_EOL,FILE_APPEND);

    }else{
        file_put_contents($log_file,$data.PHP_EOL);   //创建文件并写入

    }

}