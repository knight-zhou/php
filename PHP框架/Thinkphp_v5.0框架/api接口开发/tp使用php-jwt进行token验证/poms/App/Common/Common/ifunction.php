<?php
/**
* +-------------------------------------------------------
* |公共控制器
* |此后的模块，如果需要判断是否登录的化，直接继承此控制器
* |此控制器直接继承核心控制器
* +-------------------------------------------------------
*/
namespace Home\Controller;
use Think\Controller;

class DengluController extends Controller{
    // 定义构造方法
    public function _initialize(){
        if ( !session('?uname')){     // 找不到session,就进行页面跳转
            $this->redirect('Home/login/index');
        }
    }
    

}
