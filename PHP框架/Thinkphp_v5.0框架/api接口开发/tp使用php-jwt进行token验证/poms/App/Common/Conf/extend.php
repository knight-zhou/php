<?php
return array(
    'name'=>'knight',
    'city'=>'cs',
    'person'=>array('sex'=>'man','age'=>'28'),

    // 上传配置参数设置
    'config'=>array(
        'rootPath'=>'./Uploads_File/',
        'maxSize'=>'3145728',
        'exts' => array('zip', 'sh', 'py', 'pl'),
        'savePath' => './script/',
        'saveName'=>'',
        'replace' => 'replace',
        'autoSub' => false,
    ),

    // 其他配置

);