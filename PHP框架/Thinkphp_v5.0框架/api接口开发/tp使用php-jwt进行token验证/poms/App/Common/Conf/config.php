<?php
return array(
	// 开发阶段关闭缓存
	'TMPL_CACHE_ON' => false,
	'HTML_CACHE_ON'=> false,
	'DB_FIELD_CACHE'=>false,

	
	// mysql配置
	'DB_TYPE'   => 'mysql',
	// 'DB_HOST'   => '192.168.100.11',
	'DB_HOST'   => 'localhost',
	'DB_NAME'   => 'poms',
	'DB_NAME'   => 'poms',
	'DB_USER'   => 'poms', 
	'DB_PWD'    => 'poms2018', 
	'DB_PORT'   => '3306', 
	'DB_PREFIX' => 'tk_', 

	// 设置session
	'SESSION_OPTIONS' => array('path'=>'D:/tmp/sessions/'),

	// 设置Cookie 路径
	'COOKIE_PATH'           =>  'D:/tmp/cookie/',     // Cookie路径

    /* 日志设置 */
    'LOG_RECORD'            =>  true,   // 默认不记录日志
    'LOG_TYPE'              =>  'File', // 日志记录类型 默认为文件方式
    'LOG_LEVEL'             =>  'EMERG,ALERT,CRIT,ERR',   // 允许记录的日志级别
    'LOG_FILE_SIZE'         =>  2097152,	// 日志文件大小限制
    'LOG_EXCEPTION_RECORD'  =>  true,    // 是否记录异常信息日志


	// 加载多个自定义公共函数
	'LOAD_EXT_FILE' => 'ifunction,mfunction,logrecord',

    // 底部位置,加载扩展配置文件
	'LOAD_EXT_CONFIG' => 'extend',
	
	// 设置jwt 的原始字符串
	'jwt_key'=>"abcde",


	
);

