<?php
// 访问:  http://127.0.0.1/tp5/public/index.php/Index/Index/index

// http://127.0.0.1/tp5/public/index.php/Index/index/api/sno/0001

namespace app\index\controller;


use think\Db;   // 引入数据库类
use think\Controller;   // 引入控制器类,首字母应该大小
use \app\admin\controller\Index as AdminIndex;   // 用于跨域控制器调用

use app\index\model\Student;

class Index extends controller
{
    public function index()
    {
        
        // return '我们的龙哥哥很帅';
        $data=Db::table('user')->select();
        // var_dump($data);
    // 加载页面
        $this->assign('data',$data);

        return view();
    }


    public function api($sno='0001'){
        $data=Student::getBysno($sno);  //得到信息
        return json($data);

    }



}
