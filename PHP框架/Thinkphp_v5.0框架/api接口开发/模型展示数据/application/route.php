<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

/*
return [
    '__pattern__' => [
        'name' => '\w+',
    ],
    '[hello]'     => [
        ':id'   => ['index/hello', ['method' => 'get'], ['id' => '\d+']],
        ':name' => ['index/hello', ['method' => 'post']],
    ],

];
*/
// http://127.0.0.1/tp5/public/index.php/test
// 引入系统类
use think\Route;

// 定义路由规则，静态路由
Route::rule('/','index/index/index');   // index 模块下的index控制器下的index方法
Route::rule('test','index/index/test');

// 带参数路由(带一个)
// Route::rule('aa/:id','index/index/aa');
// 带两个参数
Route::rule('aa/:year/:month','index/index/time');
// 带可选参数路由
Route::rule('aa/:year/[:month]','index/index/time');
Route::rule(':a/:b','index/index/dongtai');

// 完全匹配路由
Route::rule('test1','index/index/test');
Route::rule('test2','index/index/test2?a=fdsaf&b=fdasf');


