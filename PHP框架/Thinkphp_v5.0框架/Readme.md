## 目录结构
```
application  应用目录 是整个网站的核心
    -- index 前台目录
		-- controller  控制器
		-- model       数据模型
		-- view        页面
	-- admin 后台目录
extend  扩展类目录
public  静态资源和入口文件
    -- staic  存放静态资源 css,js,img
	-- index.php  入口文件
runtime 网站运行时临时目录
thinkphp  TP框架的核心文件
    -- lang 语言包
	.....
vendor  第三方扩展目录
```
## URL 地址了解
http://www.tp.com/  index.php   /Index       /Index    /index

  域名                入口文件  前台(模块)   控制器     方法

### 了解TP开发模式
* 开启调试模式 application/config.php
* 连接数据库   application/database.php
* 控制器中编写代码
* 视图中编写代码

## 模块(以前台为例)
* 1 模块地址: tp5/application/index
* 2 作用: 网站所有前台相关都与其有关


## 模块(以后台为例)
```
a 后台控制器目录(tp5/application/admin/controller)下新建User.php
b 在控制器中书写代码
c 地址栏访问(http://127.0.0.1/tp5/public/index.php/Admin/Index/index)
```


## 控制器中编写代码
* 1 控制器目录:  tp5/application/index/controller
* 2 作用: 书写业务逻辑
* 3 新建控制器(前台为例)
```
a 前台控制器目录(tp5/application/index/controller)下新建User.php
b 在控制器中书写代码
c 地址栏访问(http://127.0.0.1/tp5/public/index.php/Index/User/index)
d 注意: 
	1. 控制器的文件名首字母必须大写
	2. 控制器中必须声明命名空间
	3. 控制器中类名必须和文件名一致
	
```
## 操作(方法)
```php
    public function test(){
            print_r('test');

    }
```

## 模型
* 1. 数据模型地址: tp5/application/index/model
* 2. 作用： 负责数据库相关的处理\

## 视图(页面)
* 视图地址: tp5/application/index/view
* 作用： 就是网站页面

## 命名空间
* 1 与目录有关(以前台index 控制器命名空间为例)
```php
tp5/application/index/controller
namespace app\index\controller;
```

## 跨控制器调用(很牛逼)
三种方式：
* 使用命名空间
* 使用use
* 使用系统方法
```php
use \app\admin\controller\Index as AdminIndex;   // 用于跨域控制器调用

/*
	中间代码省略
*/

//http://127.0.0.1/tp5/public/index.php/Index/Index/diaoyong
    // 调用user控制器的方法
    // 第一种方式，调用当前模块的控制器的方法
    public function diaoyong(){
        $model = new \app\index\controller\User;
        echo $model->index();
    // 第二种方式,使用user
        echo "############";
    $model= new User;
    echo $model->index();
    echo "-----------------";
    // 第三种方式，使用系统方法
    $model = controller('User');
    echo $model->index();
    }

    // 调用后台模块的方法

    public function diaoyongs(){
        // 第一种
        $model=new \app\admin\controller\Index;
        echo $model->index();
        print_r("############");
        // 同上第二种
        $model = new AdminIndex();
        echo $model->index();
        print_r('-----------------');
        // 同上 第三种
        $model = controller('Admin/Index');
        echo $model->index();

    }
```
## 调用方法(本模块为例)
* 1. 调用当前看那个之前的test的方法,是用面向对象的技术
```php
$this->test();  // 调用当前控制器的方法
   or
echo self::test();  // 使用self
```
* 2. 使用系统方法
```php
 echo self::test();
```
#### 调用本模块下的另外控制器的方法 或者调用另外模块下的控制器的方法
* 使用命名空间
```php
$model = new \app\index\controller\User;   // 另外模块的方法 或者本模块下的另外控制器的方法
echo $model->index();
```
* 使用系统方法
```
echo action('User/index');    // 斜杠不要写反了要这样写
```

## 主要分析TP执行流程
* 1 入口文件
* 2 加载框架引导文件
* 3 加载基础引导文件
......

## 读取配置
```php
// use \think\Config;  // 也可以直接引入系统类
    public function test(){
         echo \think\Config::get('app_debug');  //通过系统类读取
         print_r('########');    
         echo config('app_debug');  // 通过系统函数
		 dump(config()); // 读取所有配置
		 echo config('database.type');  // 读取扩展配置
		 echo config('sex');   // 读取场景配置,前提需要在tp5/application 下新建home.php
		echo \think\Env::get('person.name1');  // 读取环境配置，也可以在首行采用use方法引入
            // print_r('test');

    }
```
## 路由
强制模式:
```php
// http://127.0.0.1/tp5/public/index.php/test
// 引入系统类
use think\Route;

// 定义路由规则
Route::rule('/','index/index/index');   // index 模块下的index控制器下的index方法
Route::rule('test','index/index/test');

// 带参数路由(带一个)
Route::rule('aa/:id','index/index/aa');   

// 带两个参数,路由设置两个就必须带两个
Route::rule('aa/:year/:month','index/index/time');    //函数部分通过 echo input('year').' '.input('month');
// 带可选参数
Route::rule('aa/:year/[:month]','index/index/time');
// 完全匹配路由
Route::rule('test1','index/index/test');

// 完全匹配路由
Route::rule('test1','index/index/test');
Route::rule('test2','index/index/test2?a=fdsaf&b=fdasf');   // 通过 dump(input()) 打印
```
## 设置路由的请求方式
* 支持get请求
```php
Route::rule('type','index/index/test','get')
or
Route::get('type','index/index/test')
```
* 支持gost请求
```php
Route::rule('type','index/index/test','post')
Route::post('type','index/index/test')
```
* 同时支持get和post
```php
Route::rule('type','index/index/test','get/post')   // 函数用dump(input()); 打印所有 
```
## 批量注册路由
```php
Route::rule([
	"test"=>"index/index/test",
	"courese/:id"=>"index/index/course"
],'','get/post')
```




